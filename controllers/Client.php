<?php

/**
* Clase con información de ayuda
*/
namespace Mvc\Controller;

class Client
{
  public function __construct()
    {
        require_once "../models/Client.php";
        $this->model = new \Mvc\Model\Client;
    }
    public function index()
    {
       $this->model->get();

        $rows = $this->model->getRows();
        require '../views/client/index.php'; 
    }
    public function delete($id)
    {
        //procedimiento de borrado
        $this->model->delete($id);
        //reenvio de url
        header('Location: /client/index');
//        echo "Borrar datos del registro actual ($id)";
    }
    public function new()
    {
        require '../views/client/new.php';
    }
    public function insert()
    {
//        echo "Insertar registro nuevo (en $_POST, envio desde new)";
        $this->model->new();
        header('Location: /client/index');
    }
    public function edit($id)
    {
        echo "Formulario de edición con datos antiguos";
        $row = $this->model->getById($id);
        include "../views/client/edit.php";
    }
    public function update()
    {
        $this->model->update();
        header('Location: /client/index');
    }

    public function search()
    {
        $this->model->search($_POST['word']);
        $rows = $this->model->getRows();
        require '../views/client/index.php'; 
    }  
}
