<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="/css/default.css">
    <title>Framework MVC. Curso 2016/2017</title>
</head>
<body>
<div id="header">

<div id="title">Framework MVC. Curso 2016/2017</div>
<div>
    <a href="/index">Inicio</a>
    <a href="/login">Login</a>
    <a href="/help">Ayuda</a>
    <a href="/client">Cliente</a>
    <a href="/login/index">Log In</a>
    <a href="/login/out">Log Out</a>

    <?php 
    session_start();
    echo $_SESSION["name"];
    ?>
</div>
</div>